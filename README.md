# **Tips to control your energy costs**

Here are few tips to make your home more comfortable, easier and relax. These tips will definitely helpful for using clean, renewable energy to power your home:

* To manage your cooling and heating systems efficiently, install a programmable thermostat to low your utility bills costs.
* Try to dry your dishes with natural air rather than using dishwasher's drying cycle.
* Plug your electronic appliances such TVs and DVD players with power strips and always switch off the strips when the appliances are not using.
* Low your thermostat water heater to 120°F.
* Try to fully close your windows and doors while heating or cooling your home.

Angele Ziehm - Energy consultant at https://energycasino.com/no/