/*
  Energy cost saving tool

   + http://england.shelter.org.uk/get_advice/downloads_and_tools/energy_costs_saving_tool

  Bitbucket:

   + https://bitbucket.org/ShelterWeb/energy-cost-saving-tool

*/

(function () {
    'use strict';

    var module = angular.module('smoothScroll', []);

    var resolveOffset = function (offset, element, options) {
        if (typeof offset == 'function') offset = offset(element, options);

        if (angular.isElement(offset)) {
            var offsetEl = angular.element(offset)[0];

            if (typeof offset != 'undefined') offset = offsetEl.offsetHeight;
        }

        return offset;
    };

    // Smooth scrolls the window to the provided element.
    //
    var smoothScroll = function (element, options) {
        options = options || {};

        // Options
        var duration = options.duration || 800,
            offset = resolveOffset(options.offset || 0, element, options),
            easing = options.easing || 'easeInOutQuart',
            callbackBefore = options.callbackBefore || function () {},
            callbackAfter = options.callbackAfter || function () {};



        var getScrollLocation = function () {
            return window.pageYOffset ? window.pageYOffset : document.documentElement.scrollTop;
        };

        setTimeout(function () {
            var startLocation = getScrollLocation(),
                timeLapsed = 0,
                percentage, position;

            // Calculate the easing pattern
            var easingPattern = function (type, time) {
                if (type == 'easeInQuad') return time * time; // accelerating from zero velocity
                if (type == 'easeOutQuad') return time * (2 - time); // decelerating to zero velocity
                if (type == 'easeInOutQuad') return time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time; // acceleration until halfway, then deceleration
                if (type == 'easeInCubic') return time * time * time; // accelerating from zero velocity
                if (type == 'easeOutCubic') return (--time) * time * time + 1; // decelerating to zero velocity
                if (type == 'easeInOutCubic') return time < 0.5 ? 4 * time * time * time : (time - 1) * (2 * time - 2) * (2 * time - 2) + 1; // acceleration until halfway, then deceleration
                if (type == 'easeInQuart') return time * time * time * time; // accelerating from zero velocity
                if (type == 'easeOutQuart') return 1 - (--time) * time * time * time; // decelerating to zero velocity
                if (type == 'easeInOutQuart') return time < 0.5 ? 8 * time * time * time * time : 1 - 8 * (--time) * time * time * time; // acceleration until halfway, then deceleration
                if (type == 'easeInQuint') return time * time * time * time * time; // accelerating from zero velocity
                if (type == 'easeOutQuint') return 1 + (--time) * time * time * time * time; // decelerating to zero velocity
                if (type == 'easeInOutQuint') return time < 0.5 ? 16 * time * time * time * time * time : 1 + 16 * (--time) * time * time * time * time; // acceleration until halfway, then deceleration
                return time; // no easing, no acceleration
            };

            // Get document height
            var getDocHeight = function () {
                return Math.max(
                document.body.scrollHeight, document.documentElement.scrollHeight,
                document.body.offsetHeight, document.documentElement.offsetHeight,
                document.body.clientHeight, document.documentElement.clientHeight);
            };

            // Calculate how far to scroll
            var getEndLocation = function (element) {
                var location = 0;
                if (element.offsetParent) {
                    do {
                        location += element.offsetTop;
                        element = element.offsetParent;
                    } while (element);
                }
                location = Math.max(location - offset, 0);
                return location;
            };

            var endLocation = getEndLocation(element);
            var distance = endLocation - startLocation;


            var scrollableElements, userScrollListener;
            if (options.stopOnUserScroll) {
                scrollableElements = angular.element([document, window]);
                userScrollListener = function (e) {
                    clearInterval(runAnimation);
                    scrollableElements.off('wheel', userScrollListener);
                };

                scrollableElements.on('wheel', userScrollListener);
            }

            // Stop the scrolling animation when the anchor is reached (or at the top/bottom of the page)
            var stopAnimation = function () {
                var currentLocation = getScrollLocation(),
                    documentHeight = getDocHeight();
                if (position == endLocation || currentLocation == endLocation || ((window.innerHeight + currentLocation) > documentHeight)) {
                    if (options.stopOnUserScroll) {
                        scrollableElements.off('wheel', userScrollListener);
                    }
                    clearInterval(runAnimation);
                    callbackAfter(element);
                }
            };


            // Scroll the page by an increment, and check if it's time to stop
            var animateScroll = function () {
                timeLapsed += 16;
                percentage = (timeLapsed / duration);
                percentage = (percentage > 1) ? 1 : percentage;
                position = startLocation + (distance * easingPattern(easing, percentage));
                window.scrollTo(0, position);
                stopAnimation();
            };


            // Init
            callbackBefore(element);
            var runAnimation = setInterval(animateScroll, 16);
        }, 0);
    };

    // Expose the library via a provider to allow default options to be overridden
    //
    module.provider('smoothScroll', function () {

        function noop() {}
        var defaultOptions = {
            duration: 800,
            offset: 0,
            easing: 'easeInOutQuart',
            callbackBefore: noop,
            callbackAfter: noop
        };

        return {
            $get: function () {
                return function (element, options) {
                    smoothScroll(element, angular.extend({}, defaultOptions, options));
                };
            },
            setDefaultOptions: function (options) {
                angular.extend(defaultOptions, options);
            }
        };
    });


    // Scrolls the window to this element, optionally validating an expression
    //
    module.directive('smoothScroll', ['smoothScroll', function (smoothScroll, smoothScrollProvider) {
        return {
            restrict: 'A',
            scope: {
                callbackBefore: '&',
                callbackAfter: '&'
            },
            link: function ($scope, $elem, $attrs) {
                if (typeof $attrs.scrollIf === 'undefined' || $attrs.scrollIf === 'true') {
                    setTimeout(function () {

                        var callbackBefore = function (element) {
                            if ($attrs.callbackBefore) {
                                var exprHandler = $scope.callbackBefore({
                                    element: element
                                });
                                if (typeof exprHandler === 'function') {
                                    exprHandler(element);
                                }
                            }
                        };

                        var callbackAfter = function (element) {
                            if ($attrs.callbackAfter) {
                                var exprHandler = $scope.callbackAfter({
                                    element: element
                                });
                                if (typeof exprHandler === 'function') {
                                    exprHandler(element);
                                }
                            }
                        };

                        var options = {
                            callbackBefore: callbackBefore,
                            callbackAfter: callbackAfter
                        };

                        if (typeof $attrs.duration != 'undefined') options.duration = $attrs.duration;

                        if (typeof $attrs.offset != 'undefined') options.offset = $attrs.offset;

                        if (typeof $attrs.easing != 'undefined') options.easing = $attrs.easing;

                        smoothScroll($elem[0], options);
                    }, 0);
                }
            }
        };
    }]);


    // Scrolls to a specified element ID when this element is clicked
    //
    module.directive('scrollTo', ['smoothScroll', function (smoothScroll) {
        return {
            restrict: 'A',
            scope: {
                callbackBefore: '&',
                callbackAfter: '&'
            },
            link: function ($scope, $elem, $attrs) {
                var targetElement;

                $elem.on('click', function (e) {
                    e.preventDefault();

                    targetElement = document.getElementById($attrs.scrollTo);
                    if (!targetElement) return;

                    var callbackBefore = function (element) {
                        if ($attrs.callbackBefore) {
                            var exprHandler = $scope.callbackBefore({
                                element: element
                            });
                            if (typeof exprHandler === 'function') {
                                exprHandler(element);
                            }
                        }
                    };

                    var callbackAfter = function (element) {
                        if ($attrs.callbackAfter) {
                            var exprHandler = $scope.callbackAfter({
                                element: element
                            });
                            if (typeof exprHandler === 'function') {
                                exprHandler(element);
                            }
                        }
                    };

                    var options = {
                        callbackBefore: callbackBefore,
                        callbackAfter: callbackAfter
                    };

                    if (typeof $attrs.duration != 'undefined') options.duration = $attrs.duration;

                    if (typeof $attrs.offset != 'undefined') options.offset = $attrs.offset;

                    if (typeof $attrs.easing != 'undefined') options.easing = $attrs.easing;

                    smoothScroll(targetElement, options);

                    return false;
                });
            }
        };
    }]);

}());
// Include angulartics, GTM and smoothScroll
var app = angular.module('bgetApp', ['angulartics', 'angulartics.google.tagmanager', 'smoothScroll']);

app.directive('showData', function ($compile) {
    return {
        scope: true,
        link: function (scope, element, attrs) {
            var el;

            attrs.$observe('template', function (tpl) {
                if (angular.isDefined(tpl)) {
                    // compile the provided template against the current scope
                    el = $compile(tpl)(scope);

                    // stupid way of emptying the element
                    element.html("");

                    // add the template content
                    element.append(el);
                }
            });
        }
    };
});

app.directive('suppliers', function () {
    return {
        template: '<div><select ng-model="mySupplier" ng-options="supplier.name for supplier in suppliers"><option value="">Find details of your supplier\'\s scheme</option></select><div style="margin: 17px 0;"><a target="_blank" ng-href="{{mySupplier.link}}" analytics-on analytics-event="{{tool.name}}" analytics-category="{{content.title}}" analytics-label="{{mySupplier.name}}">{{mySupplier.linkName}}</a></div></div>'
    };
});

// Turn into an array filter e.g:
//    todo in todos | toArray

app.filter('toArray', function () {
    'use strict';

    return function (obj) {
        if (!(obj instanceof Object)) {
            return obj;
        }
        var result = [];

        angular.forEach(obj, function (obj, key) {
            obj.$key = key;
            result.push(obj);
        });

        return result;
    };
});

app.filter('getIndex', function () {
    return function (input, id) {
        var i = 0,
            len = input.length;
        for (; i < len; i++) {
            if (+input[i].id == +id) {
                return i;
            }
        }
        return null;
    };
});

/* Get an item from an object which has a specific ID */
app.filter('getById', function () {
    return function (input, id) {
        var i = 0,
            len = input.length;
        for (; i < len; i++) {
            if (+input[i].id == +id) {
                return input[i];
            }
        }
        return null;
    };
});

/* Delete an item from an object which has a specific answer */
app.filter('deleteByAnswer', function () {
    return function (input, answer) {
        var i = 0,
            len = input.length;
        for (; i < len; i++) {
            if (input[i].answer == answer) {
                return input.splice(i, 1);
            }
        }
        return null;
    };
});

/* Get an item from an object which has a specific answer */
app.filter('getByAnswer', function () {
    return function (input, answer) {
        var i = 0,
            len = input.length;
        for (; i < len; i++) {
            if (input[i].answer == answer) {
                return true;
            }
        }
        return null;
    };
});

/* Get an item from an object which has a specific type */
app.filter('getByType', function () {
    return function (input, type) {
        var i = 0,
            len = input.length;
        for (; i < len; i++) {
            if (input[i].type == type) {
                return input[i];
            }
        }
        return null;
    };
});

/* Get an item from an object which is in a specific questionnaire */
app.filter('getByQuestionnaire', function () {
    return function (input, questionnaire) {
        var i = 0,
            len = input.length,
            tmp = [];
        for (; i < len; i++) {
            if (input[i].questionaire == questionnaire) {
                tmp.push(input[i]);
            }
        }
        return tmp;
        //return null;
    };
});

/* Get an item from an object which has a specific ID */
app.filter('deleteById', function () {
    return function (input, id) {
        var i = 0,
            len = input.length;
        for (; i < len; i++) {
            if (+input[i].id == +id) {
                return input.splice(i, 1);
            }
        }
        return null;
    };
});

/* Main controller for the application */
app.controller('MainController', function ($scope, $http, $filter, $sce, $timeout, $analytics, $location, smoothScroll, $compile) {

    if (typeof String.prototype.startsWith != 'function') {
        // see below for better implementation!
        String.prototype.startsWith = function (str) {
            return this.indexOf(str) === 0;
        };
    }

    function checkPathways(required, contentKey, versionKey) {

        angular.forEach($scope, function (scope, key) {

            if (key.startsWith("otherPathway") || key == "pathway") {

                if (angular.equals(scope, required)) {

                    $scope.contents[contentKey].available = true;
                    $scope.contents[contentKey].versions[versionKey].available = true;

                }

            }

        });

    }

    $scope.editAnswer = function (questionID, questionIndex) {

        if (questionIndex === 0) {
            $scope.answers = [];
            $scope.pathway = [];
        }

        if (questionIndex + 1 < $scope.answers.length) {

            angular.forEach($scope.questions, function (question, key) {

                if (key >= questionIndex) {

                    var indie = $filter('getIndex')($scope.answers, question.id);

                    $scope.answers.splice(indie, 1);

                }

                if (key === questionIndex) {
                    $scope.questions[questionIndex].state = true;
                }

            });

        } else {

            var indie = $filter('getIndex')($scope.answers, questionID);

            $scope.answers.splice(indie, 1);
            $scope.questions[questionIndex].state = true;

        }

    };

    $scope.secure = $sce.trustAsHtml;

    $scope.contentAboutToLoad = false;

    function compileSelect() {
        $timeout(function () {
            var tpl = $compile("<div suppliers=''></div>")($scope);
            document.getElementById("selectGoesHere").appendChild(tpl[0]);
            document.getElementById("selectGoesHere").id = "";

        }, 1000);
    }

    $scope.pathway = [];

    $scope.getAboveAfterChildren = function (currentIndex) {

        if (currentIndex !== 0) {

            var i = currentIndex - 1;

            if (typeof $scope.questions[i].afterChildren === "number") {
                return $scope.questions[i].afterChildren;
            }

            return i;

        }

        return currentIndex;

    };


    $scope.getAboveChildren = function (currentIndex) {

        var i = currentIndex - 1;

        if (currentIndex !== 0 && $scope.questions[i].hasOwnProperty("children")) {

            return $scope.questions[i].children.length;

        }

        return 0;

    };

    $scope.contentLoaded = false;

    $scope.childAnswer = {};

    /* watch the answers object and return the new and old value */
    $scope.$watch('answers', function (newValue, oldValue) {

        if ($scope.answers.length !== 0) {

            var currentAnswers = newValue;

            // Loop through all the contents

            $scope.typeOfUser = [{
                id: $scope.typeOfUser[0].id,
                answer: $scope.typeOfUser[0].answer
            }];

            angular.forEach($scope.contents, function (obj, key) {

                // If the answers object matches the contents required array

                if (angular.equals(obj.required, currentAnswers)) {

                    // Set the content to available
                    $scope.contents[key].available = true;

                } else {

                    if (angular.equals(obj.required, $scope.typeOfUser)) {

                        $scope.contents[key].available = true;

                    } else {

                        // It did not match, so set it to false
                        $scope.contents[key].available = false;

                        // Loop through each different version of the content
                        angular.forEach(obj.versions, function (version, vkey) {

                            version.available = false;

                            // If this version's required array matches the answers
                            if (angular.equals(version.required, currentAnswers)) {

                                // Set the content to available
                                $scope.contents[key].available = true;
                                version.available = true;


                            }
                            if (angular.equals(version.required, $scope.typeOfUser) || angular.equals(version.required, $scope.pathway)) {

                                // Set the content to available
                                $scope.contents[key].available = true;
                                version.available = true;


                            } else {

                                checkPathways(version.required, key, vkey);

                            }
                        });
                    }

                }
            });

        }

    });

    // Check if a question with a specific ID is in the answers object
    $scope.isInAnswer = function (id, answer, nopathway) {
        var found = $filter('getById')($scope.answers, id);

        if (found) {
            if (typeof answer !== "number") {
                if (found.answer === answer) {
                    return true;
                } else if (typeof answer === "undefined") {
                    return true;
                }
            } else {
                var question = $filter('getById')($scope.questions, id);
                if (found.answer == question.options[answer].name) {
                    return true;
                }
            }
        } else {

            if (!nopathway) {



                var find = $filter('getById')($scope.pathway, id);

                if (find) {

                    if (typeof answer !== "number") {

                        if (find.answer === answer) {
                            return true;
                        } else if (typeof answer === "undefined") {
                            return true;
                        }

                    }
                }

                return false;

            }

        }

    };


    // Check if a question with a specific ID is in the answers object
    $scope.isInPathway = function (id, answer) {
        var found = $filter('getById')($scope.pathway, id);

        if (found) {
            if (typeof answer !== "number") {
                if (found.answer === answer) {
                    return true;
                } else if (typeof answer === "undefined") {
                    return true;
                }
            } else {
                var question = $filter('getById')($scope.questions, id);
                if (found.answer == question.options[answer].name) {
                    return true;
                }
            }
        } else {

            var find = $filter('getById')($scope.pathway, id);

            if (find) {

                if (typeof answer !== "number") {
                    if (find.answer === answer) {
                        return true;
                    } else if (typeof answer === "undefined") {
                        return true;
                    }
                }
            }

            return false;

        }


    };

    // Check if two objects match
    $scope.match = function (ob1, ob2) {
        if (angular.equals(ob1, ob2)) {
            return true;
        }
        return false;
    };

    // Get all the suppliers
    $http.get('/test_area/michael/tools/data/suppliers/_nocache').success(function (data) {
        $scope.suppliers = data.suppliers;
    });

    // When they are off to check a supplier
    $scope.mySupplier = "";
    $scope.goToSupplier = function (mySupplier) {

        window.location.href = mySupplier.link;

    };

    $scope.questionCount = 0;

    $scope.goingToEmail = false;
    $scope.emailadvice = [];

    $scope.print = function () {
        $scope.goingToEmail = false;
        window.print();
    };

    $scope.tips = [];

    var toolId = 2,
        questionnaireId = 4;

    var getById = $filter('getById'),
        getByQuestionnaire = $filter('getByQuestionnaire'),
        getByType = $filter('getByType'),
        getByAnswer = $filter('getByAnswer');

    $http.get('https://bitbucket.org/ShelterWeb/tools-data/raw/master/bget.json').success(function (data) {

        $timeout(function () {

            $scope.contentLoaded = true;

        }, 350);

        $scope.tool = data.tool;
        $scope.questionnaires = data.questionnaires,
        $scope.questions = data.questions,
        $scope.contents = data.contents;

        $scope.contents = orderBy($scope.contents, 'type', false);

        $scope.countQs = 0;

        angular.forEach($scope.questions, function (obj, key) {
            $scope.countQs++;
            if (typeof obj.children !== undefined) {
                angular.forEach(obj.children, function (child, ckey) {
                    $scope.countQs++;
                });
            }
        });

    });

    $scope.contentAvailable = false;
    $scope.contentToDisplay = [];

    var orderBy = $filter('orderBy');

    $scope.answers = [];

    var scrollOptions = {
        duration: 750,
        easing: 'easeInQuad',
        offset: 120,
        callbackBefore: function (element) {
            console.log('about to scroll to element', element);
        },
        callbackAfter: function (element) {
            console.log('scrolled to element', element);
        }
    };

    $scope.oneAnswer = function (questionID, optionName, $event, questionName, theIndex, type, questionIndex, childIndex, optIndex) {

        var found = $filter('getById')($scope.answers, questionID),
            next = questionIndex + 1;

        if (type == "child" && found === null) {

            var temp = [$filter('getById')($scope.answers, $scope.questions[questionIndex].id)];

            temp.push({
                "id": questionID,
                    "answer": optionName
            });

            $scope.questions[questionIndex].children[childIndex].state = false;

            $scope.pathway = $filter('orderBy')(temp, 'id', false);

        } else if (found === null) {

            $scope.answers.push({
                "id": questionID,
                    "answer": optionName
            });

            $scope.questions[questionIndex].state = false;

            if (questionIndex !== 0) {

                $scope["otherPathway" + questionIndex] = angular.copy($scope.pathway);

                $scope["otherPathway" + questionIndex].push({
                    "id": questionID,
                        "answer": optionName
                });

                $scope["otherPathway" + questionIndex] = $filter('orderBy')($scope["otherPathway" + questionIndex], 'id', false);

            }

        }

        if (questionIndex === 0 && type !== "child" && found === null) {

            $scope.answers = [{
                "id": questionID,
                    "answer": optionName
            }];

            $scope.typeOfUser = [{
                "id": questionID,
                    "answer": optionName
            }];

            $scope.questions[questionIndex].state = false;

        } else {

            compileSelect();

            $scope.contentAboutToLoad = true;

            $timeout(function () {

                $scope.contentAboutToLoad = false;

            }, 750);

        }

        $scope.answers = orderBy($scope.answers, 'id', false);

        if ($scope.questions[questionIndex].children.length !== 0 && type !== "child" && optionName === "Renting") {

            $timeout(function () {
                var ele = document.getElementById("question" + questionIndex + "-0");
                smoothScroll(ele);
            }, 250);

        } else {

            if (questionName == "Do you claim benefits or tax credits?") {

                $timeout(function () {
                    var ele = document.getElementById("content-introduction");
                    smoothScroll(ele);
                }, 850);

            } else {

                $timeout(function () {
                    var ele = document.getElementById("question" + next);
                    smoothScroll(ele);
                }, 250);

            }

        }

    };

    $scope.objContains = function (option) {

        return getByAnswer($scope.answers, option);

    };

});
